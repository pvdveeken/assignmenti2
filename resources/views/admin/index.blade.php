@extends('layouts.app')
@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Admin Panel</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('adminDelete') }}">
						{!! csrf_field() !!}

						<div class="form-group">
							<h4><label class="col-md-12 control-label" style="text-align: left">Delete All Messages</label></h4>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-warning">
									<i class="fa fa-2x fa-trash"></i>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection