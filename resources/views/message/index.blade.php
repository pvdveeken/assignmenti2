@extends('layouts.app')
@section('content')
	@forelse($messages as $message)
		@include('message.showPartial', ['messsage' => $message])
	@empty
		<div class="row">
			<div class="col-md-12" style="text-align: center">
				<h3>No messages to display</h3>
			</div>
		</div>
	@endforelse
@endsection