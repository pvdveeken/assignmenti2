<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-md-12">
						<div class="pull-left">
							<i class="fa fa-btn fa-user" title="Posted By"></i><strong>{{$message->user->name}}</strong>
						</div>
						<div class="pull-right">
							{{$message->created_at->diffForHumans()}}
						</div>
					</div>
				</div>
			</div>
			<div class="panel-body">
				{!! Markdown::convertToHtml($message->body) !!}
			</div>
		</div>
	</div>
</div>