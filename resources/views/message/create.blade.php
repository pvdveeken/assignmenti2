@extends('layouts.app')
@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Post New Message</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('messageStore') }}">
						{!! csrf_field() !!}
						
						<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
							<h4><label class="col-md-1 control-label">Message</label></h4>
							<div class="col-md-12">
								<textarea class="form-control" name="body" rows="3">{{ old('body') }}</textarea>
								
								@if ($errors->has('body'))
									<span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="pull-left"><a href="https://guides.github.com/features/mastering-markdown/" target="_blank">
									Styling with Markdown is supported
								</a></div>
								<div class="pull-right">
									<button type="submit" class="btn btn-success">
										<i class="fa fa-btn fa-plus"></i>Post
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection