## Configuratie ##
Oké, allereerst moet je deze repo clonen naar je projectfolder en deze commands runnen:

```
#!bash
$ cd map-waarin-je-de-repo-gecloned-hebt
$ composer install
```
Vervolgens moet je homestead draaiende krijgen, om dit voor elkaar te krijgen kan je het beste [deze tutorial](https://laracasts.com/series/laravel-5-fundamentals/episodes/2) te volgen. Als je composer e.d nog niet geïnstalleerd hebt, moet je eerst ff [deze aflevering](https://laracasts.com/series/laravel-5-fundamentals/episodes/1) kijken. In aflevering 2, bij de stap dat je homestead.yaml aan moet passen, moet je naar de map wijzen waarin je de repo gecloned hebt. 

Als het gelukt is om homestead draaiend te krijgen, zijn er nog twee commands die je moet runnen:

```
#!bash
$ php artisan migrate
$ php artisan db:seed
```
Het eerste command maakt alle benodigde tables aan in de database, het tweede command populeert de database. Je moet maar even in de bestanden database/seeds/PermissionTableSeeder.php en database/seeds/RoleTableSeeder.php kijken, dan wordt het meteen duidelijk wat er gebeurt. 

Als het goed is heb je nu een werkende versie van het project. Waarschijnlijk is het wel ff handig om een paar van [deze lessons](https://laracasts.com/series/laravel-5-fundamentals) te kijken om een beetje een begrip van laravel te krijgen. De leercurve is namelijk nogal steil in het begin.
## Het Project ##
De volgende eisen heb ik al werkend gekregen:

* user creation form with name and password, password must be hashed
* user login form
* the first user that signs up is the admin
* post message form, the message can use Markdown
* show messages in a list, include the author username, messages render the Markdown
* an admin page that can remove all current messages or make them invisible

**Alles Af! **