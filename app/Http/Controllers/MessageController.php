<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
	/**
	 * Attach auth middleware to the controller, so
	 * that only logged-in users can access it.
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('permission:post-messages', ['only' => ['create', 'store']]);
	}

	/**
	 * Show a listing of all posted messages.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$messages = Message::orderBy('created_at', 'desc')->get();

		return view('message.index', ['messages' => $messages]);
	}

	/**
	 * Show the form for creating a new Message.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		return view('message.create');
	}

	/**
	 * Store a new message in the database.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'body' => 'required|min:5|max:255'
		]);
		Message::create([
			'body'      => $request->get('body'),
		   'user_id'   => Auth::user()->id
		]);
		return redirect(route("messageIndex"));
	}
}