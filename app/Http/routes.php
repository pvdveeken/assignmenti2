<?php

use App\Message;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::model('message', 'App\Message');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function ()
{
	Route::resource('message', 'MessageController', [
		'names' => [
			'index'  => 'messageIndex',
			'show'   => 'messageShow',
			'create' => 'messageCreate',
			'store'  => 'messageStore'
		],
		'only'   => ['index', 'show', 'create', 'store']
	]);
});
Route::group(['middleware' => 'web'], function ()
{
	Route::auth();

	Route::get('/admin', ['as' => 'admin', 'middleware' => 'permission:administrate', function()
	{
		return view('admin.index');
	}]);
	Route::post('/admin', ['as' => 'adminDelete', 'middleware' => 'permission:administrate', function()
	{
		Message::destroy(Message::all(['id'])->pluck('id')->toArray());

		return redirect(route("home"));
	}]);
	Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
	Route::get('/home', function()
	{
		return redirect(route("home"));
	});
});
