<?php

use Illuminate\Support\Facades\Route;

function setActive($routeName, $routeParam = null, $class = 'active')
{
	if(is_array($routeName))
	{
		$return = "";

		foreach($routeName as $key => $value)
		{
			$return .= setActive($value, $routeParam);
		}
		return $return;
	}
	if($routeName != Route::currentRouteName())
	{
		return "";
	}
	$params = Route::current()->parameters();
	if(!empty($params) && $routeParam != null)
	{
		if(array_keys($params) != array_keys($routeParam))
		{
			return "";
		}
		foreach($params as $key => $value)
		{
			if($value->id != $routeParam[$key]->id)
			{
				return "";
			}
		}
	}
	return $class;
}