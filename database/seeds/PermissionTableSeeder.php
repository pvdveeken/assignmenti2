<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;

class PermissionTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$post = Permission::create([
			'name'          =>  'post-messages',
			'display_name'  =>  'Post Messages',
			'description'   =>  'Is allowed to post messages to the site.'
		]);
		$administrate = Permission::create([
			'name'          =>  'administrate',
			'display_name'  =>  'Administrate',
			'description'   =>  'Is allowed to administrate the site.'
		]);
		$admin = Role::where('name', 'admin')->get()->first();
		$user = Role::where('name', 'user')->get()->first();

		$admin->attachPermission($post);
		$admin->attachPermission($administrate);
		$user->attachPermission($post);
	}
}
