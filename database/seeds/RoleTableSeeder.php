<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Role::create([
			'name'         => 'admin',
			'display_name' => 'Administrator',
			'description'  => 'Administrates the website'
		]);
		Role::create([
			'name'         => 'user',
			'display_name' => 'User',
			'description'  => 'Just an ordinary user...'
		]);
	}
}
